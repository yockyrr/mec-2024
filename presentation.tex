\documentclass[aspectratio=169]{beamer}
\usepackage{pdfpc-movie}
\usepackage{tikz}
\usetheme{Boadilla}
\usecolortheme{dove}

\title{The ``Clausula Archive of the Notre Dame Repertory''}
\subtitle{End-to-end OMR, encoding, and analysis of medieval polyphony}
\author{Joshua Stutter}
\institute{University of Sheffield}
\date{April 22, 2024}
\titlegraphic{
  \includegraphics[width=0.8\textwidth]{W1_00072_crop.png}
}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}
\section{Introduction and Terminology}
\begin{frame}{Introduction and Terminology}
  \begin{description}
    \item[\textbf{Clausulae}] (Substitute \textit{clausulae}) Small, alternate sections of \textit{organum}, predominantly in \textit{discantus}
    \item[\textbf{Conductus}] Predominantly note-to-note, no tenor
    \item[\textbf{Cum littera}] Style of notation used in syllabic, note-to-note music (motet, \textit{conductus}). Rhythm inferred from the prosody of the text
    \item[\textbf{Discantus}] Style of \textit{organum} with all parts in strict modal rhythm
    \item[\textbf{Motet}] Early motets were typically prosulated \textit{clausulae}, both Latin and vernacular texts
    \item[\textbf{Organum}] Florid polyphony in two or more parts, with a plainchant tenor
    \item[\textbf{Organum per se}] \textit{Purum} without regular patterning in the organal part
    \item[\textbf{Organum purum}] Style of \textit{organum} with a long, held tenor
    \item[\textbf{Sine littera}] Style of notation used in all types of \textit{organum}, as well as motet tenors
  \end{description}
\end{frame}
\begin{frame}{Not the full story\ldots}
  The \textit{clausula} is fundamental to understanding musical reuse in the thirteenth century. However\ldots
  \begin{itemize}
    \item Examples of \textit{clausulae} deriving from motets rather than the other way around (Rokseth 1939; Bradley 2013)
    \item Interrelationships between \textit{organa} without the help of \textit{clausulae} (Smith)
    \item Links to \textit{conductus} (Bukofzer; Everist)
  \end{itemize}
  The \textit{clausula} is not the full story for musical reuse. Musical reuse must go deeper.
\end{frame}
\section{Analysis of medieval polyphony}
\begin{frame}{Analysis of medieval polyphony}
  \begin{itemize}
    \item `Despite their anonymity, motets are increasingly treated not \textit{en masse}, or in large taxonomical blocks, but as individual works, highly fashioned by skilled composers, deeply embedded in complex networks of other voices, musical works, genres, and poetry' (Leach 2019, p.134)
    \item It is possible to apply close reading to instances of polyphony where their status as stable or written transmission is in no doubt
    \item But it is difficult to apply this methodology across the repertory, or in cases of extreme variation
    \item Large questions remain:
      \begin{itemize}
        \item What does it mean to share musical material in Notre Dame polyphony?
        \item How deep does this musical reuse penetrate?
        \item Are such distinctions as ``similar'' vs ``different'' meaningful?
      \end{itemize}
  \end{itemize}
\end{frame}
\section{Notational and technological hurdles}
\begin{frame}{The Clausula Archive of the Notre Dame Repertory (CANDR)}
  \begin{itemize}
    \item Doctoral research project, 2019--2023, studying musical reuse in the Notre Dame repertory
    \item For creating, managing, encoding, and analysing pre- and proto-mensural polyphonic datasets
    \item Two components:
      \begin{itemize}
        \item Database and web application (candr.org.uk):
          \begin{itemize}
            \item Browsing and editing the sources of the Notre Dame repertory
            \item Open source dataset
            \item Manual input as well as Optical Music Recognition (OMR)
          \end{itemize}
        \item \textit{Python} analysis package:
          \begin{itemize}
              \item Analysing Big Data polyphonic corpora
              \item Importing data from CANDR
              \item Novel methodologies for extracting patterns of musical reuse in arhythmic polyphonic music
          \end{itemize}
      \end{itemize}
    \end{itemize}
\end{frame}
\begin{frame}{Difficulties editing thirteenth-century polyphony}
  \begin{itemize}
    \item No prescriptive rhythm: rhythm is inferred from contextual clues and performative interpretation
    \item Only the simplest \textit{discantus} is clearly in one mode throughout
    \item ``Rules'' of modal rhythm are frequently bent or broken (\textit{fractio modi})
    \item Many passages can be read equally well in two different rhythmic modes
    \item Some passages of the ``same'' music are re-ligated in different manuscripts, indicating different rhythmic interpretations
    \item Older editorial practice tried its best to read everything in rhythm (Waite 1954)
    \item Modern practice leaves rhythm largely uninterpreted
    \item Alignment is also often unclear and subjective
    \item `It is not that the transcription is wrong, merely that it does not acknowledge the latent ambiguity in the source it is transcribed from' (Bell 2006, pp.315-6)
  \end{itemize}
\end{frame}
\section{Solutions in CANDR}
\begin{frame}{Graphical vs Symbolic}
  \centering
  \begin{tabular}{l|l}
    \textbf{Graphical} & \textbf{Symbolic} \\
    \hline
    Facsimile & Edition \\
    Automatic notation construction & Human editor \\
    One-to-one symbolic ``conversion'' & Expert edition \\
    Graphical data & Transcription, critical apparata \\
  \end{tabular}
  \\ [12pt]
  \includegraphics[width=0.34\textwidth]{W1_00111_crop.png}
  \hfill
  \includegraphics[width=0.64\textwidth]{ta.png}
\end{frame}
\begin{frame}{Interface}
  \begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{candrbrowsestave.png}
    \caption{Browsing the tracing-transcription of a stave using the CANDR interface}
  \end{figure}
\end{frame}
\begin{frame}{Solutions in CANDR}
  \begin{itemize}
    \item Avoiding the typical unspoken understanding that the graphical domain can be represented as a single symbolic domain
    \item There are many ways in which the graphical Notre Dame notation can be interpreted symbolically
    \item Dispenses with ``transcription'' in favour of ``representation''
    \item Stores only graphical data generated by OMR and manual input, plus editorial tagging
    \item Currently provides one symbolic representation using a customised MEI encoding
    \item Symbolic representations generated on-the-fly to keep up to date with the graphical data
    \item Current contents of the database:
      \begin{itemize}
        \item Two whole sources
        \item 898 settings of polyphony, 9,710 staves of music, 560,293 notational elements
        \item Alignments between staves for settings in score format
        \item Comprehensive set of metadata
      \end{itemize}
  \end{itemize}
\end{frame}
\section{Analysis package}
\begin{frame}{Analysis package}
  \begin{itemize}
    \item Packages such as \textit{jSymbolic} and \textit{music21} do not support ambiguous notations
    \item ``Chordifying'' the music does not work either, as we cannot say for certain which elements are aligned
    \item Ignoring the polyphonic context entirely is counterproductive to the features that we want to analyse
    \item CANDR converts the notation to a directed graph, and then uses the flattened adjacency matrix as a vector
    \item 67 unique symbolic elements = vector of length 134 (each link = 2 features)
    \item Visualised by decomposition using Principal Component Analysis (PCA)
  \end{itemize}
  \centering
  \includegraphics[width=0.9\textwidth]{pipeline.pdf}
\end{frame}
\section{Initial results}
\begin{frame}
  \frametitle{The whole data set}
  \centering
  \pdfpcmovie[autostart, loop]{\includegraphics[width=0.8\textwidth]{boomerang_whole_set_rotate_nosound_edit_withlegend.png}}{boomerang_whole_set_rotate_nosound_edit_withlegend.mp4}
\end{frame}
\begin{frame}
  \frametitle{The divisione element}
  \centering
  \pdfpcmovie[autostart, loop]{\includegraphics[width=0.8\textwidth]{boomerang_divisione_rotate_v2_nosound_withlegend.png}}{boomerang_divisione_rotate_v2_nosound_withlegend.mp4}
\end{frame}
\begin{frame}
  \frametitle{Distribution of clefs}
  \centering
  \pdfpcmovie[autostart, loop]{\includegraphics[width=0.6\textwidth]{boomerang_clefs_rotate_nosound_edit.png}}{boomerang_clefs_rotate_nosound_edit.mp4}
\end{frame}
\begin{frame}
  \frametitle{Distribution of ligated vs unligated notes}
  \centering
  \pdfpcmovie[autostart, loop]{\includegraphics[width=0.8\textwidth]{boomerang_ligated_unligated_rotate_nosound_withlegend.png}}{boomerang_ligated_unligated_rotate_nosound_withlegend.mp4}
\end{frame}
\begin{frame}
  \frametitle{Whole-setting reuse}
  \centering
  \pdfpcmovie[autostart, loop]{\includegraphics[width=0.8\textwidth]{boomerang_95_472_427_655_rotate_nosound_edit_withlegend.png}}{boomerang_95_472_427_655_rotate_nosound_edit_withlegend.mp4}
\end{frame}
\begin{frame}
  \frametitle{Stylometric analysis}
  \centering
  \pdfpcmovie[autostart, loop]{\includegraphics[width=0.8\textwidth]{boomerang_chancellor_655_656_175_229_213_243_244_245_VS_unrelated_655_654_658_663_664_VS_48_crucifigat_rotate_withlegend.png}}{boomerang_chancellor_655_656_175_229_213_243_244_245_VS_unrelated_655_654_658_663_664_VS_48_crucifigat_rotate_withlegend.mp4}
\end{frame}
\section{Limitations and conclusions}
\begin{frame}{Limitations and conclusions}
  \begin{itemize}
    \item Limitations:
      \begin{itemize}
        \item Not yet a complete process for analysing musical reuse, still a relatively blunt tool
        \item We want to be able to measure these patterns to determine significance
        \item May be remedied by a more robust embedding methodology
      \end{itemize}
    \item Conclusions:
      \begin{itemize}
        \item Largest and most comprehensive database of Notre Dame polyphony currently available, entirely open source
        \item Successful representation rather than transcription, reproducible and transparent
        \item Developing analytical frameworks for ``distant reading'' of thirteenth-century polyphony
      \end{itemize}
  \end{itemize}
\end{frame}
\end{document}
